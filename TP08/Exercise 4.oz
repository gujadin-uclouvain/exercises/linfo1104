declare Xs S F
fun  {Producer N}
    fun {ProducerAcc A N}
        {Delay 1000}
        {Browse 'Created'}
        if A == N then A|nil
        else A|{ProducerAcc A+1 N}
        end
    end
in
    {ProducerAcc 1 N}
end

fun {Filter Str}
    case Str
    of H|T then
        if H mod 2 == 0 then {Filter T}
        else {Browse H} H|{Filter T}
        end
    else nil
    end
end

fun {Consumer Str}
    fun {SumAcc Str A}
        case Str
        of nil then A
        [] H|T then {Browse A} {SumAcc T A+H}
        end
    end
in
    {SumAcc Str 0}
end

thread Xs = {Producer 12} end
thread F = {Filter Xs} end
thread S = {Consumer F} end
{Browse S}