declare
fun {ProduceInts N}
    fun {ProdcuceIntsAcc N A}
        {Delay 1000}
        if N == A then A|_
        else A|{ProdcuceIntsAcc N A+1} end
    end
in
    {ProdcuceIntsAcc N 1}
end 

declare
fun {Sum Str}
    fun {SumAcc Str A}
        case Str
        of H|T then {Browse H+A} {SumAcc T H+A}
        end
    end
in
    {SumAcc Str 0}
end

declare Xs S
Xs = thread {ProduceInts 5} end
{Browse Xs}
thread S = {Sum Xs} end
{Browse S}

declare Xs S
Xs = {ProduceInts 5}
S = {Sum Xs}
{Browse S}
