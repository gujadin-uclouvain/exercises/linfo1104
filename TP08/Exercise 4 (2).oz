declare
fun {Producer N}
    fun {ProducerAcc N A}
        {Delay 1000}
        {Browse created}
        if A == N then N|_
        else A|{ProducerAcc N A+1}
        end
    end
in
    {ProducerAcc N 1}
end