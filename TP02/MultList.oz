declare
fun{MultListAcc L A}
  if L.2 == nil then A*L.1
  else {MultListAcc L.2 A*L.1}
  end
end

fun{MultList L}
  {MultListAcc L 1}
end

{Browse {MultList [1 2 3 4]}}