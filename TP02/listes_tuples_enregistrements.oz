declare
%Listes
{Browse {IsList '|'(a '|'(b nil))}}
{Browse {IsList '|'(2:nil a)}}
{Browse {IsList [a b c]}}

% Tuples
{Browse {IsTuple '|'(a b)}} 
{Browse {IsTuple state(1 a 2)}}
{Browse {IsTuple state(1 3:2 2:a)}}
{Browse {IsTuple a#b#c}}
{Browse {IsTuple m|n|o}}

% Enregistrements
{Browse {IsTuple tree(v:a t1 t2)}}