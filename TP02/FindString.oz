declare
fun{Prefix L1 L2}
  if L1 == nil then true
  elseif L2 == nil then false
  elseif L1.1 == L2.1 then {Prefix L1.2 L2.2}
  else false
  end
end

fun{FindStringIndex S T Index}
  if T == nil then nil
  elseif {Prefix S T} == true then Index|{FindStringIndex S T.2 Index+1}
  else {FindStringIndex S T.2 Index+1}
  end
end

fun{FindString S T}
  {FindStringIndex S T 1}
end

{Browse {FindString [c] [a b a b a b]}}