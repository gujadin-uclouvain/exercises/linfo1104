declare
L1=a|nil
L2=a|(b|c|nil)|d|nil
L3=proc {$} {Browse oui} end | proc {$} {Browse non} end | nil
L4=est|une|liste|nil
L5=(a|p|nil)|nil

fun{Head L}
   L.1
end

fun{Tail L}
   L.2
end

fun{Length L A}
   if L==nil then A
   else {Length L.2 A+1}
   end
end

fun{Append L1 L2}
   if L1==nil then L2
   else L1.1|{Append L1.2 L2}
   end
end

fun{Append2 L1 L2}
    case L1
    of H1|_ then H1|{Append2 L1.2 L2}
    [] nil then
        case L2
        of H2|_ then H2|{Append2 L1 L2.2}
        [] nil then nil
        end
    end
end

fun{Append3 L1 L2}
    case L1
    of H1|_ then H1|{Append3 L1.2 L2}
    [] nil then L2
    end
end

fun{Head2 L}
   case L
   of H|T then H
   else L
   end
end

declare
Carte = carte(menu(entree: 'salade verte aux lardons'
		   plat: 'steak frites'
		   prix: 10)
	      menu(entree: 'salade de crevettes grises'
		   plat: 'saumon fume et pommes de terre'
		   prix: 12)
	      menu(plat: 'choucroute garnie'
		   prix: 9))


{Browse Carte.2.plat}