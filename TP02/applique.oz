declare
fun{Applique2 L F}
    case L
    of nil then nil
    [] (H|T) then {F H}|{Applique T F}
    end 
end

fun{AppliqueAcc L F NewL Iter}
    case L
    of nil then {Flatten NewL|nil}
    [] (H|T) then
        if Iter =< 0 then {AppliqueAcc T F {F H} Iter+1}
        else {AppliqueAcc T F [NewL {F H}] Iter+1}
        end
    end
end

fun{Applique L F} {AppliqueAcc L F nil 0} end
fun {Lol X} lol(X) end


proc{MakeAdder N ?Add}
    proc{Add X Z}
        Z=X+N
    end
end


fun{AddAll L N}
    {Applique L {MakeAdder N}}
end

{Browse {AddAll [1 2 3 4 5 6 7 8 9 10] 42}}