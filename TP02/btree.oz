declare
fun{Promenade2 BT}
    if     BT.left \= empty andthen BT.right \= empty then BT.1|{Promenade BT.left}|{Promenade BT.right}
    elseif BT.left \= empty andthen BT.right == empty then BT.1|{Promenade BT.left}
    elseif BT.left == empty andthen BT.right \= empty then BT.1|{Promenade BT.right}
    else BT.1|nil
    end
end

declare
fun {Promenade BT}
  case BT
  of empty then nil
  [] btree(V left:L right:R) then V|{Promenade L}|{Promenade R}
  % else BT.1|{Promenade BT.left}|{Promenade BT.right}
  end
end

fun{FlattenPromenade BT}
  {Flatten {Promenade BT}}
end

{Browse
{FlattenPromenade
btree(42
left: btree(26
left: btree(54
left: empty
right: btree(18
left: empty
right: empty))
right: empty)
right: btree(37
left: btree(11
left: empty
right: empty)
right: empty))}}