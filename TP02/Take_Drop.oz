declare
fun{Take Xs N}
  if Xs == nil orelse N =< 0 then nil
  else Xs.1|{Take Xs.2 N-1}
  end
end

fun{Drop Xs N}
  if Xs == nil then nil
  elseif N =< 0 then Xs
  else {Drop Xs.2 N-1}
  end
end

{Browse {Take [r [a p] h] ~3}}