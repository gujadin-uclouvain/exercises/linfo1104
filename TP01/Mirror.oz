declare
fun {Mirror N A}
   if N==0 then A
   else {Mirror (N div 10) ((A*10)+(N mod 10))}
   end
end

{Browse {Mirror 12345 0}}