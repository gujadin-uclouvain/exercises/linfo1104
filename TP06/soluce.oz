% Exemple de solutions pour le TP6
% par Sebastien Kalbusch

%1
declare
fun {Reverse Xs} Y in
   Y = {NewCell nil}
   for X in Xs do
      Y := X|@Y
   end
   @Y
end
{Browse {Reverse [5 3 2 1 0]}}

% Identique en terme d'appels rec
% L'etat n'est pas encapsule ! Il y a une Cell Y
% Xs est une liste (immutable), il ne peut pas changer !

%2
fun {NewStack}
   {NewCell nil}
end
fun {IsEmpty S}
   @S == nil
end
proc {Push S X}
   S := X|@S
end
fun {Pop S}
   if {IsEmpty S} then nil
   else R in
      R = @S.1
      S := @S.2
      R
   end
end

fun {Eval Xs}
   S = {NewStack}
in
   for X in Xs do
      case X
      of '+' then
	 {Push S {Pop S} + {Pop S}}
      [] '-' then Op1 Op2 in
	 Op1 = {Pop S}
	 Op2 = {Pop S}
	 {Push S Op2 - Op1}
      [] '*' then
	 {Push S {Pop S} * {Pop S}}
      [] '/' then Op1 Op2 in
	 Op1 = {Pop S}
	 Op2 = {Pop S}
	 {Push S Op2 / Op1}
      [] N then {Push S N}
      end
   end
   {Pop S}
end

{Browse {Eval [13 45 '+' 89 17 '-' '*']}} % affiche 4176 = (13+45)*(89-17)
{Browse {Eval [13 45 '+' 89 '*' 17 '-']}} % affiche 5145 = ((13+45)*89)-17
{Browse {Eval [13 45 89 17 '-' '+' '*']}} % affiche 1521 = 13*(45+(89-17))

%3
declare
fun {NewStack}
   S
   fun {IsEmpty}
      @S == nil
   end
   proc {Push X}
      S := X|@S
   end
   fun {Pop}
      if {IsEmpty} then nil
      else R in
	 R = @S.1
	 S := @S.2
	 R
      end
   end
in
   S = {NewCell nil}
   stack(isEmpty:IsEmpty push:Push pop:Pop)
end

Stack1={NewStack}
Stack2={NewStack}

{Browse {Stack1.isEmpty}}
{Stack1.push 13}
{Browse {Stack1.isEmpty}}
{Browse {Stack2.isEmpty}}
{Stack1.push 45}
{Stack2.push {Stack1.pop}}
{Browse {Stack2.isEmpty}}
{Browse {Stack1.pop}}

fun {Eval Xs}
   S = {NewStack}
in
   for X in Xs do
      case X
      of '+' then
	 {S.push {S.pop} + {S.pop}}
      [] '-' then Op1 Op2 in
	 Op1 = {S.pop}
	 Op2 = {S.pop}
	 {S.push Op2 - Op1}
      [] '*' then
	 {S.push {S.pop} * {S.pop}}
      [] '/' then Op1 Op2 in
	 Op1 = {S.pop}
	 Op2 = {S.pop}
	 {S.push Op2 / Op1}
      [] N then {S.push N}
      end
   end
   {S.pop}
end

{Browse {Eval [13 45 '+' 89 17 '-' '*']}} % affiche 4176 = (13+45)*(89-17)
{Browse {Eval [13 45 '+' 89 '*' 17 '-']}} % affiche 5145 = ((13+45)*89)-17
{Browse {Eval [13 45 89 17 '-' '+' '*']}} % affiche 1521 = 13*(45+(89-17))

% Cette pile est un objet (représenté par un record)
% car on a groupe valeur et operations.
% On aurait pu faire un Abstract Data Type.

%4
declare
fun {Shuffle Xs} Len A I Head Tail in
   Len = {Length Xs}
   I = {NewCell 0}
   A = {NewArray 0 Len 0}
   for X in Xs do
      A.@I := X
      I := @I+1
   end
   Tail = {NewCell Head}
   for N in 0..(Len-1) do Idx Range Next in
      Range = Len - 1 - N
      Idx = {OS.rand} mod (Range+1)
      @Tail = A.Idx|Next
      Tail := Next
      A.Idx := A.Range
   end
   @Tail = nil
   Head
end
{Browse {Shuffle [0 1 2 3]}}

%5
declare
C = {NewCell nil}
L1={NewCell 0}|{NewCell 1}|{NewCell 2}|nil
L2=0|{NewCell 1|{NewCell 2|C}}
% L1 est une liste de Cell
% L2 n'est pas une liste car ne termine pas par nil

%O(1)
fun {Prepend1 X L} X|L end
fun {Prepend2 X L} X|{NewCell @L} end

% pour ajouter à la fin de L1 on utilise Append O(n)
% pour L2 O(n)
proc {Append2 L X}
   case L
   of _|T then {Append2 T X}
   else
      case @L of nil then L := X|{NewCell nil}
      [] _|C then {Append2 C X} end
   end
end
{Append2 L2 3}
{Browse @C}

declare
% both are unsafe, we need at least two elements
fun {Swap L} L.2.1|L.1|L.2.2 end
fun {Swap2 L} H T in
   H = (@(L.2)).1
   T = (@(L.2)).2
   L.2 := L.1|T
   H|L.2
end

%6
declare
class Collection
   attr elements
   meth init
      elements := nil
   end
   meth put(X)
      elements := X|@elements
   end
   meth get($)
      case @elements of X|Xr then elements := Xr X end
   end
   meth isEmpty($)
      @elements == nil
   end
   meth union(C)
      if {Not {C isEmpty($)}} then
	 {self put({C get($)})}
	 {self union(C)}
      end
   end
   meth toList($)
      @elements
   end
end

declare
class SortedCollection from Collection
   meth put(X)
      fun {PutAux L}
	 case L of nil then X|nil
	 [] H|T then
	    if X =< H then X|L
	    else H|{PutAux T} end
	 end
      end in
      elements := {PutAux @elements}	 
   end
end

declare % tests
C1 = {New Collection init}
C2 = {New SortedCollection init}

{C1 put(0)}
{C1 put(1)}
{C2 put(2)}
{C2 put(1)}
{C2 put(3)}

{C1 union(C2)}
{Browse {C2 isEmpty($)}}
{Browse {C1 isEmpty($)}}
{Browse {C1 toList($)}}

% if C1, C2 are Collections, union is O(|C2|)
% because put is O(1)

% if C1, C2 are SortedCollections,
% union is O(|C2|^2 + |C1|*|C2|)

% proof:
% (n1 + (n1+1) + (n1+2) + ... + (n1+n2-1))
% ~= (n1 + (n1+1) + (n1+2) + ... + (n1+n2))
% = n2*n1 + (0+1+2+...+n2)
% = n2*n1 + 0.5*n2*(n2+1)
% ~= n2*n1 + 0.5*n2*n2
% => O(n2^2 + n2*n1)

declare
Xs = [7 8 0 4 3]
C3 = {New SortedCollection init}
for X in Xs do
   {C3 put(X)}
end
{Browse {C3 toList($)}}
% insertion sort O(n^2)

%7
declare
class Constante
   attr value
   meth init(V) value := V end
   meth evalue($) @value end
   meth derive(V $) {New Constante init(0)} end
end
class Variable
   attr value
   meth init(V) value := V end
   meth evalue($) @value end
   meth derive(V E)
      if self == V then E = {New Constante init(1)}
      else E = {New Constante init(0)} end
   end
   meth set(V) value := V end 
end
class Somme
   attr e1 e2
   meth init(Expr1 Expr2)
      e1 := Expr1
      e2 := Expr2
   end
   meth evalue($) {@e1 evalue($)} + {@e2 evalue($)} end
   meth derive(V $)
      {New Somme init({@e1 derive(V $)} {@e2 derive(V $)})}
   end
end
class Difference
   attr e1 e2
   meth init(Expr1 Expr2)
      e1 := Expr1
      e2 := Expr2
   end
   meth evalue($) {@e1 evalue($)} - {@e2 evalue($)} end
   meth derive(V $)
      {New Difference init({@e1 derive(V $)} {@e2 derive(V $)})}
   end
end
class Produit
   attr e1 e2
   meth init(Expr1 Expr2)
      e1 := Expr1
      e2 := Expr2
   end
   meth evalue($) {@e1 evalue($)} * {@e2 evalue($)} end
   meth derive(V $) D1 D2 in
      D1 = {New Produit init({@e1 derive(V $)} @e2)}
      D2 = {New Produit init({@e2 derive(V $)} @e1)}
      {New Somme init(D1 D2)}
   end
end
class Puissance
   attr e1 c
   meth init(Expr1 C)
      e1 := Expr1
      c := C
   end
   meth evalue($) {Pow {@e1 evalue($)} @c} end
   meth derive(V $) De1 P CP in
      De1 = {@e1 derive(V $)}
      P = {New Puissance init(@e1 @c-1)}
      CP = {New Produit init({New Constante init(@c)} P)} 
      {New Produit init(CP De1)}
   end
end

declare
VarX={New Variable init(0)}
VarY={New Variable init(0)}
local
   ExprX2={New Puissance init(VarX 2)}
   Expr3={New Constante init(3)}
   Expr3X2={New Produit init(Expr3 ExprX2)}
   ExprXY={New Produit init(VarX VarY)}
   Expr3X2mXY={New Difference init(Expr3X2 ExprXY)}
   ExprY3={New Puissance init(VarY 3)}
in
   Formule={New Somme init(Expr3X2mXY ExprY3)}
end

{VarX set(7)}
{VarY set(23)}
{Browse {Formule evalue($)}} % affiche 12153
{VarX set(5)}
{VarY set(8)}
{Browse {Formule evalue($)}} % affiche 547

declare
Derivee={Formule derive(VarX $)} % represente 6x - y
{VarX set(7)}
{VarY set(23)}
{Browse {Derivee evalue($)}} % affiche 19

%8
declare
class Sequence % not very efficient !
   attr l
   meth init(L) l := L end
   meth isEmpty($) @l == nil end
   meth first($) {List.nth @l 1} end
   meth last($) {List.last @l} end
   meth insertFirst(X) l := X|@l end
   meth insertLast(X) l := {List.append @l X|nil} end
   meth removeFirst l := {List.drop @l 1} end
   meth removeLast
      if {self isEmpty($)} then skip
      else l := {List.take @l {List.length @l}-1} end
   end
end

fun {Palindrome Xs}
   S
   fun {Check}
      if {S isEmpty($)} then true
      elseif {S first($)} == {S last($)} then
	 {S removeFirst}
	 {S removeLast}
	 {Check}
      else false end
   end
in
   S = {New Sequence init(Xs)}
   {Check}
end
{Browse {Palindrome "radar"}}
{Browse {Palindrome "abc"}}
{Browse {Palindrome "a"}}
{Browse {Palindrome "eluparcettecrapule"}}