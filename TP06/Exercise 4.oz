declare
L = [1 2 3 4 5 6 7 8 9 10]

fun {Random N} {OS.rand} mod N + 1 end % renvoie un nombre aleatoire entre 1 et N

fun {ListToArray L}
    A = {NewArray 1 {Length L} 0}
    fun {FillArray L I A}
        case L
        of nil then A
        [] H|T then 
            A.I := H 
            {FillArray T I+1 A}
        end
    end
in
    {FillArray L 1 A}
end

fun {Shuffle Xs}
    A = {ListToArray Xs}
    fun {Shuffle2 A N}
        local Rand Temp in
            if N == 0 then nil
            else
                Rand = {Random N}
                Temp = A.Rand
                A.Rand := A.N
                Temp|{Shuffle2 A N-1}
            end
        end
    end
in
    {Shuffle2 A {Length Xs}}
end

{Browse {Shuffle L}}