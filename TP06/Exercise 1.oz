declare
fun {Reverse2 Xs}
    fun {ReverseAux Xs Ys}
        case Xs
        of nil then Ys
        [] X|Xr then {ReverseAux Xr X|Ys}
        end
    end
in
    {ReverseAux Xs nil}
end

Y = {NewCell nil}
proc {Reverse L}
    for I in L do
        Y := I|@Y
    end
end

{Browse {Reverse2 [1 2 3 4 5]}}

{Reverse [1 2 3 4 5]}
{Browse @Y}