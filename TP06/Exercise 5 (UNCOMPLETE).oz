declare
End = {NewCell nil}
L1 = {NewCell 0}|{NewCell 1}|{NewCell 2}|nil
L2 = 0|{NewCell 1|{NewCell 2|End}}

fun {Read1 L}
    case L of nil then nil
    [] C|T then @C|{Read1 T}
    end
end

fun {Read2 L}
    case L of nil then nil
    [] H|C then H|{Read2 @C}
    end
end

fun {AddFirst1 L X}
    {NewCell X}|L
end

fun {AddFirst2 L X}
    X|{NewCell L}
end

fun {AddLast1 L X}
    case L
    of nil then {NewCell X}|nil
    [] H|T then H|{AddLast1 T X}
    end
end

proc {AddLast2 L X}
    case L of _|T then {AddLast2 T X}
    else
        case @L of nil then L := X|{NewCell nil}
        [] _|C then {AddLast2 C X}
        end
    end
end

fun {Swap1 L} L.2.1|L.1|L.2.2 end
fun {Swap2 L} H T in
    H = (@(L.2)).1
    T = (@(L.2)).2
    L.2 := L.1|T
    H|L.2
end
{Browse '!!!!!!!!!!!!!!!'}
{Browse {Read1 {AddFirst1 L1 ~1}}}
{Browse {Read2 {AddFirst2 L2 ~1}}}
{Browse '---------------'}
{Browse {Read1 {AddLast1 L1 3}}}
{AddLast2 L2 3} {Browse {Read2 L2}}
{Browse '---------------'}
{Browse {Read1 {Swap1 L1}}}
{Browse {Read2 {Swap2 L2}}}
{Browse '!---------------!'}