declare
fun {NewStack}
    {NewCell nil}
end

fun {IsEmpty S}
    @S == nil
end

proc {Push S X}
    S := X|@S
end

fun {Pop S}
    local R in
        R = @S.1
        S := @S.2
        R
    end
end

fun {Eval L}
    local S in
        S = {NewStack}
        for I in L do
            case I
            of '+' then {Push S ({Pop S} + {Pop S})}
            [] '-' then {Push S (~{Pop S} + {Pop S})}
            [] '*' then {Push S ({Pop S} * {Pop S})}
            [] '/' then {Push S ((1/{Pop S}) / (1/{Pop S}))}
            else {Push S I}
            end
        end
        {Pop S}
    end
end

{Browse {Eval [13 45 '+' 89 17 '-' '*']} == 4176}
{Browse {Eval [13 45 '+' 89 '*' 17 '-']} == 5145}
{Browse {Eval [13 45 89 17 '-' '+' '*']} == 1521}
