declare
fun {NewStack}
    local S in
        S = {NewCell nil}
        fun {IsEmpty} @S == nil end
        proc {Push X} S := X|@S end
        fun {Pop} local R in R = @S.1  S:= @S.2  R end end
    end
in
    stack(isEmpty:IsEmpty push:Push pop:Pop)
end

fun {Eval L}
    local S in
        S = {NewStack}
        for I in L do
            case I
            of '+' then {S.push ({S.pop} + {S.pop})}
            [] '-' then {S.push (~{S.pop} + {S.pop})}
            [] '*' then {S.push ({S.pop} * {S.pop})}
            [] '/' then {S.push ((1/{S.pop}) / (1/{S.pop}))}
            else {S.push I}
            end
        end
        {S.pop}
    end
end

{Browse {Eval [13 45 '+' 89 17 '-' '*']} == 4176}
{Browse {Eval [13 45 '+' 89 '*' 17 '-']} == 5145}
{Browse {Eval [13 45 89 17 '-' '+' '*']} == 1521}