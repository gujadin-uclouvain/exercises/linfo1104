declare
fun {MakeBinaryGate F}
    fun {$ Xs Ys}
        fun {GateLoop Xs Ys}
            case Xs#Ys
            of (X|Xr)#(Y|Yr) then {F X Y}|{GateLoop Xr Yr}
            end
        end
    in
        thread {GateLoop Xs Ys} end
    end
end

declare
AndG={MakeBinaryGate fun {$ X Y} X*Y end}
OrG={MakeBinaryGate fun {$ X Y} X+Y-X*Y end}
NandG={MakeBinaryGate fun {$ X Y} 1-X*Y end}
NorG={MakeBinaryGate fun {$ X Y} 1-X-Y+X*Y end}
XorG={MakeBinaryGate fun {$ X Y} X+Y-2*X*Y end}

local S=0|0|1|1|_ R=0|1|0|1|_ Q NotQ
    proc {Bascule Rs Ss Qs NotQs}
        {NorG Rs NotQs Qs}
        {NorG Ss 0|Qs NotQs}
    end
in
    {Bascule R S Q NotQ}
    {Browse Q#NotQ}
end
