declare
proc {ForCollect Xs P Ys}
    Acc={NewCell Ys}
    proc {C X} R2 in @Acc=X|R2 Acc:=R2 end
in
    for X in Xs do {P C X} end @Acc=nil
end

declare
proc {ForCollectDecl Xs P Ys}
    proc {C X} X end
in
    case Xs of nil then Ys = nil
    [] H|T then Ys = {P C H}|{ForCollectDecl T P}
    end
end

{Browse {ForCollectDecl [ 0 2 4 6 8 ]
    proc {$ Collect X} {Collect X div 2} end}}

declare R2 R3 Y
C = {NewCell Y}
@C = 0|1|R2
C := R2
@C = 0|1|R3
C := R3
@C = nil
{Browse Y}