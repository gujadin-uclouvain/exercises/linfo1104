% EXERCICE 3
% Complexite O(1) pour MakeBinaryGate
% Complexite O(n) pour les fonctions renvoyees
declare
fun {MakeBinaryGate F}
   fun {$ Xs Ys}
      fun {Loop S1 S2}
	 case S1#S2 of (H1|T1)#(H2|T2) then
	    {F H1 H2}|{Loop T1 T2}
	 end
      end
   in
      thread {Loop Xs Ys} end
   end
end

declare
fun {And X Y}
   X*Y
end
fun {Or X Y}
   X+Y-X*Y
end
fun {Nor X Y}
   1-{Or X Y}
end
OrGate = {MakeBinaryGate Or}
NorG = {MakeBinaryGate Nor}
AndGate = {MakeBinaryGate And}

local S1 S2 S3 S4 S5 in
   S1=1|0|1|0|_
   S2=0|1|1|0|_
   S3={AndGate S1 S2}
   S4={OrGate S1 S2}
   S5={NorGate S1 S2}
   {Browse S3}
   {Browse S4}
   {Browse S5}
end

% EXERCICE 4
local R=1|1|1|0|_ S=0|1|0|0|_ Q NotQ % il faut corriger le faute de frappe
   proc {Bascule Rs Ss Qs NotQs}
      Qs={NorG Rs NotQs}
      NotQs={NorG Ss 0|Qs} % et ajouter une val initiale pour Q (ou nonQ)
   end
in
   {Bascule R S Q NotQ}
   {Browse Q#NotQ}
end