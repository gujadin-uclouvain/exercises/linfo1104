local MakeAdd Add1 Add2 Sub1 Sub2 in
    proc {MakeAdd X ?Add ?Sub}
        proc {Add Y ?Z}
            Z=X+Y
        end
        proc {Sub Y ?Z}
            Z=X-Y
        end
    end
    {MakeAdd 1 Add1 Sub1}
    {MakeAdd 2 Add2 Sub2}

    local V W in
        {Add1 42 V} {Browse V}
        {Sub1 42 W} {Browse W}
    end

    local V W in
        {Add2 42 V} {Browse V}
        {Sub2 42 W} {Browse W}
    end
end