declare
class Counter
    attr value

    meth init % (re)initialise le compteur
        value:=0
    end

    meth inc % incremente le compteur
        value:=@value+1
    end

    meth get(X) % renvoie la valeur courante du compteur dans X
        X=@value
    end
end

MonCompteur={New Counter init}

for X in [65 81 92 34 70] do {MonCompteur inc} end

{Browse {MonCompteur get($)}} % affiche 5
