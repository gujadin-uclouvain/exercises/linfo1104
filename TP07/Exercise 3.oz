declare
class Variable
    attr Var
    meth init(X) Var := X end
    meth set(X) Var := X end
    meth evalue($) @Var end
    meth derive(V E)
        if self == V then E = {New Variable init(1)}
        else E = {New Variable init(0)} end
    end
end

class Constante
    attr Const
    meth init(X) Const := X end
    meth evalue($) @Const end
    meth derive(V E) {New Constante init(0)}
end

class Somme
    attr E1
    attr E2
    meth init(Expr1 Expr2) E1 := Expr1 E2 := Expr2 end
    meth evalue($) {@E1 evalue($)} + {@E2 evalue($)} end
    meth derive(V $) {New Somme init({@E1 derive(V $)} {@E2 derive(V $)})} end
end

class Difference
    attr E1
    attr E2
    meth init(Expr1 Expr2) E1 := Expr1 E2 := Expr2 end
    meth evalue($) {@E1 evalue($)} - {@E2 evalue($)} end
    meth derive(V $) {New Difference init({@E1 derive(V $)} {@E2 derive(V $)})} end
end

class Produit
    attr E1
    attr E2
    meth init(Expr1 Expr2) E1 := Expr1 E2 := Expr2 end
    meth evalue($) {@E1 evalue($)} * {@E2 evalue($)} end
    meth derive(V $) in V1 V2
        V1 = {New Produit init({@E1 derive(V $)} @E2)}
        V2 = {New Produit init({@E2 derive(V $)} @E1)}
        {New Somme init(V1 V2)}
    end
end

class Puissance
    attr E1
    attr Expon
    meth init(Expr Const) E1 := Expr Expon := Const end
    meth evalue($) {Pow {E evalue($)} Expon} end
    meth derive(V $) Base Exponant ExponantMult in
        Base = {@E1 derive(V $)}
        Exponant = {New Puissance init(@E1 @Expon-1)}
        ExponantMult = {New Produit init({New Constante init(@Expon)} Exponant)}
        {New Puissance init(Base ExponantMult)}
    end
end