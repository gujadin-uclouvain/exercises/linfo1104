declare
class Sequence
    attr l1 l2
    meth init(L)
        l1:=nil 
        l2:=nil 
        Length={List.length L}
        IsPaire = (Length mod 2 == 0)
        Middle
        R
        proc {F L Length Middle IsPaire ?R}
            if Length > 0 then
                if Length > Middle then 
                    if @l1==nil then l1:=[L.1] else l1:={List.append @l1 [L.1]} end 
                    {F L.2 Length-1 Middle IsPaire R}
                elseif Length == Middle andthen {Not IsPaire} then
                    l1:={List.append @l1 [L.1]} l2:=L.1|@l2 {F L.2 Length-1 Middle IsPaire R}
                elseif Length == Middle andthen IsPaire then
                    l2:=L.1|@l2 {F L.2 Length-1 Middle IsPaire R}
                else l2:=L.1|@l2 {F L.2 Length-1 Middle IsPaire R} end
            else R=done end
        end
    in
        if IsPaire then Middle=Length div 2
        else Middle=(Length div 2)+1 end
        R={F L Length Middle IsPaire}
    end

    meth isEmpty($) @l1 == nil orelse @l2 == nil end
    meth first($) @l1.1 end
    meth last($) @l2.1 end
    meth insertFirst(X) l1:=X|@l1 end
    meth insertLast(X) l2:=X|@l2 end
    meth removeFirst l1:=@l1.2 end
    meth removeLast l2:=@l2.2 end
end
    

declare
fun {Palindrome Xs}
    S
    fun {Check}
        %% si S est vide, alors Xs est un palindrome
        %% sinon, on compare les premier et dernier elements de S:
        %% - s'ils sont egaux, on les retire de S et on continue
        %% - sinon, Xs n'est pas un palindrome
        if {S isEmpty($)} then true
        elseif {S first($)} == {S last($)} then {S removeFirst} {S removeLast} {Check}
        else false end
    end
in
    S={New Sequence init(Xs)}
    {Check}
end

{Browse {Palindrome "azertyuiopqsdfghjklmwxcvbnnbvcxwmlkjhgfdsqpoiuytreza"}} %% true
{Browse {Palindrome "azertyuiopqsdfghjrlmwxcvbnnbvcxwmlkjhkfdsqpoiuytreza"}} %% false
{Browse {Palindrome "a"}}