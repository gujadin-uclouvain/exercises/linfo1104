declare
class Collection
    attr elements

    meth init
        elements := nil
    end

    meth put(X)
        elements := X|@elements
    end

    meth get($)
        case @elements
        of X|Xr then 
            elements := Xr
            X
        end
    end

    meth isEmpty($)
        @elements == nil
    end

    meth union(C)
        if {Not {C isEmpty($)}} then
            {self put({C get($)})}
            {self union(C)}
        end
    end

    meth browse
        {Browse @elements}
    end
end

class SortedCollection from Collection
    meth put(X)
        fun {PutAux L}
            case L
            of nil then X|nil
            [] H|T then
                if X >= H then
                    X|L
                else H|{PutAux T}
                end
            end
        end
    in
        elements := {PutAux @elements}
    end
end

C1 = {New SortedCollection init}
for X in [1 5 2 3 4 6 0] do {C1 put(X)} end

{C1 browse}