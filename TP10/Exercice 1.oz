declare
proc {BrowsePort S}
    {Browse S.1}
    {BrowsePort S.2}
end

declare
P S
{NewPort S P}
{Send P foo}
{Send P bar}
{BrowsePort S}

{Browse S}