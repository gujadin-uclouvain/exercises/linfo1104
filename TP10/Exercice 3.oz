declare
proc {GenerateRMIServer N}
    fun {StudentRMI}
        S in
        thread
            for ask(howmany:Beers) in S do
                Beers={OS.rand} mod 24
            end
        end
        {NewPort S}
    end
    fun {CreateUniversityRMI Size}
        fun {CreateLoop I}
            if I =< Size then
                {StudentRMI}|{CreateLoop I+1}
            else nil
            end
        end
    in
        {CreateLoop 1}
    end 
    proc {CharlotteRMI L} Res Tot Moy Min Max in
        {Browse '--RMI Server--'}
        Res = {List.map L fun{$ S} {Send S ask(howmany:$)} end}
        {Browse 'Liste Consomations'#Res}
        Tot = {List.foldL Res fun{$ X Y} X+Y end 0}
        {Browse 'Consomation Totale'#Tot}
        if Tot > 0 then
            Moy = Tot div {List.length Res}
            {Browse 'Consomation Moyenne'#Moy}
            Min = {List.foldL Res Value.min Res.1}
            {Browse 'Consomation Minimale'#Min}
            Max = {List.foldL Res Value.max Res.1}
            {Browse 'Consomation Maximale'#Max}
        end
    end
in
    {CharlotteRMI {CreateUniversityRMI N}}
end

declare
proc {GenerateCallBackServer N}
    fun {StudentCallBack}
        S in
        thread
            for ask(howmany:Port) in S do
                {Send Port {OS.rand} mod 24}
            end
        end
        {NewPort S}
    end
    fun {CreateUniversityCallBack Size}
        fun {CreateLoop I}
            if I =< Size then
                {StudentCallBack}|{CreateLoop I+1}
            else nil
            end
        end
    in
        {CreateLoop 1}
    end
    proc {CharlotteCallBack L} Res Tot Moy Min Max in
        {Browse '--CallBack Server--'}
        Res = {List.map L fun{$ P} S in {Send P ask(howmany:{NewPort S})} S.1 end}
        {Browse 'Liste Consomations'#Res}
        Tot = {List.foldL Res fun{$ X Y} X+Y end 0}
        {Browse 'Consomation Totale'#Tot}
        if Tot > 0 then
            Moy = Tot div {List.length Res}
            {Browse 'Consomation Moyenne'#Moy}
            Min = {List.foldL Res Value.min Res.1}
            {Browse 'Consomation Minimale'#Min}
            Max = {List.foldL Res Value.max Res.1}
            {Browse 'Consomation Maximale'#Max}
        end
    end
in
    {CharlotteCallBack {CreateUniversityCallBack N}}
end

{GenerateRMIServer 5}
{GenerateCallBackServer 5}