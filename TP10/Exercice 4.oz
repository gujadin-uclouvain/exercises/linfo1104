declare
fun {MakePortier}
    proc {PortierHelp S Tot}
        case S of H|T then
            case H
            of getIn(N) then {PortierHelp T Tot+N}
            [] getOut(N) then {PortierHelp T Tot-N}
            [] getCount(N) then N=Tot {PortierHelp T Tot}
            [] setCount(N) then {PortierHelp T N}
            end
        else skip
        end
    end
    S
in
    thread {PortierHelp S 0} end
    {NewPort S}
end

declare
Portier = {MakePortier}
{Send Portier getIn(10)}
{Send Portier getOut(5)}
{Send Portier setCount(60)}
{Browse {Send Portier getCount($)}}