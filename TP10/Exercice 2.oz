declare A B N S Res1 Res2 Res3 Res4 Res5 Res6
fun {LaunchServer}
    proc{MsgTreatment S}
        case S
        of add(X Y R)|T then R=(X+Y) {MsgTreatment T}
        [] pow(X Y R)|T then {Pow X Y R} {MsgTreatment T}
        [] 'div'(X Y R)|T then R=(X div Y) {MsgTreatment T}
        else {Browse "je ne comprends pas votre message"} {MsgTreatment S.2}
        end
    end
in
    local S P in
        {NewPort S P}
        thread {MsgTreatment S} end
        P
    end
end


S = {LaunchServer}
{Send S add(321 345 Res1)}
{Browse Res1}

{Send S pow(2 N Res2)}
N = 8
{Browse Res2}

{Send S add(A B Res3)}
{Send S add(10 20 Res4)}
{Send S foo}
{Browse Res4}
A = 3
B = 0-A
{Send S 'div'(90 Res3+1 Res5)}
{Send S 'div'(90 Res4 Res6)}
{Browse Res3}
{Browse Res5}
{Browse Res6}
