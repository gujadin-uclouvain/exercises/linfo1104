-module(exercise3).
-export([my_pow/0, user/1, compute_pow/0]).

my_pow() ->
	receive
		{Pid, _, B, E} -> Pid ! {self(), my_pow(B, E)}
	end,
	my_pow().
my_pow(B, E) when E >= 0 -> my_pow(B, E, 1).
my_pow(_, 0, R) -> R;
my_pow(B, E, R) -> my_pow(B, E-1, R*B).

user(PowPid) -> user(PowPid, 1, 2, 100, 0).
user(_, _, _, 0, Acc) -> io:format("Sum of my_pow's executions => ~p~n", [Acc]);
user(PowPid, B, E, Max, Acc) ->
	PowPid ! {self(), "Calculate power", B, E},
	receive
		{Pid, Answer} ->
			io:format("my_pow (~p) returns ~p~n", [Pid, Answer]),
			user(PowPid, B+1, E, Max-1, Acc+Answer);
		_ -> ok
	end.

compute_pow() ->
	Pow = spawn(?MODULE, my_pow, []),
	user(Pow).
