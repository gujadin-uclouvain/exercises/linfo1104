-module(exercise3).
-export([pow_main/0 ]).

my_pow(Base, Exp) -> my_pow(Base, Exp, 1).
my_pow(_, 0, Res) -> Res;
my_pow(Base, Exp, Res) -> my_pow(Base, Exp-1, Res*Base).
my_pow() ->
	receive
	{Pid, Base, Exp} -> Pid ! {self(), my_pow(Base, Exp)}
	end,
	my_pow().
sum_pow(_, From, To, Res) when From > To -> Res;
sum_pow(Pid, From, To, Res) ->
	Pid ! {self(), From, 2},
	receive
	{Pid, Comp} -> sum_pow(Pid, From+1, To, Res+Comp)
	end.
	sum_pow(PowPid, From, To) -> sum_pow(PowPid, From, To, 0).
pow_main() ->
	PowPid = spawn(?MODULE, my_pow, []),
	io:fwrite("~p~n", [sum_pow(PowPid, 1, 100)]).
