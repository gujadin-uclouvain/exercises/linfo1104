-module(my_math).
-export([my_add/2, start/0]).

my_add(A, B) -> A + B.

start() ->
	io:fwrite("~p~n", [my_add(2, 3)]).