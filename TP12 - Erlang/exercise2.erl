-module(exercise2).
-export([voiture/0, start/0]).

voiture() -> voiture(stopped).
voiture(State) ->
	receive
		start when State =:= stopped -> 
			io:format("Engine is started~n"),
			voiture(started);
		move when State =:= started ->
			io:format("I'm moving~n"),
			voiture(State);
		stop when State =:= started ->
			io:format("Engine is down~n"),
			voiture(stopped);
		Msg -> 
			io:format("Impossible to \"~p\" during \"~p\" state~n", [Msg, State]),
			voiture(State)
	end.

start() ->
	V = spawn(?MODULE, voiture, []), % ou spawn(?MODULE, voiture, [idle])
	V ! move,
	V ! start,
	V ! move,
	V ! start,
	V ! move,
	V ! stop,
	V ! stop.
