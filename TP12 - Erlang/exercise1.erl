-module(exercise1).
-export([my_add/2, my_pow/2, my_fib/1, run/1]).

my_add(A, B) -> A + B.

my_pow(B, E) when E >= 0 -> my_pow(B, E, 1).
my_pow(_, 0, R) -> R;
my_pow(B, E, R) -> my_pow(B, E-1, R*B).

my_fib(Nb) -> my_fib_term(0, 1, Nb).
my_fib_term(R,_,0) -> R;
my_fib_term(R,Acc,Nb) -> my_fib_term(Acc, R+Acc, Nb-1).

run(L) ->
	case L of
		[H|T] ->
			Valid_functions = [my_add, my_pow, my_fib],
			Args = lists:map(fun(Y) -> list_to_integer(atom_to_list(Y)) end, T),
			case lists:member(H, Valid_functions) of
				true ->
					io:fwrite("~p~n", [apply(exercise1, H, Args)]);
				false ->
					io:fwrite("unknown function~n")
			end;
		_ ->
			io:fwrite("unk~n")
	end.
