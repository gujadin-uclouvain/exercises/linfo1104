declare
fun{ReduceList L F Acc}
    case L
    of nil then Acc
    [] H|T then {ReduceList T F {F H Acc}}
    end
end

F = fun{$ N1 N2} N1+N2 end
G = fun{$ N1 N2} N1*N2 end

{Browse {ReduceList [1 2 3 4 5 6 7 8 9 10] F 0 }}
{Browse {ReduceList [1 2 3 4 5 6 7 8 9 10] G 1 }}