declare
fun {SumSquare N}
    fun{SumSquareAux N Acc}
        if N =< 0 then Acc
        elseif N mod 2 == 0 then {SumSquareAux N-1 Acc}
        else {SumSquareAux N-1 Acc + (N*N)} end
    end
in
    {SumSquareAux N 0}
end

%----------------------------------------------------------------------
declare
fun {GenerateList N}
    fun {GenerateListAcc N Counter}
        if Counter > N then nil
        else Counter|{GenerateListAcc N Counter+1}
        end
    end
in
    {GenerateListAcc N 0}
end

fun {MyFilter L F}
    case L
    of nil then nil
    [] H|T then 
        if {F H} then H|{MyFilter T F}
        else {MyFilter T F}
        end
    end
end

fun {MyMap L F}
    case L
    of nil  then nil
    [] H|T then {F H}|{MyMap T F}
    end
end

fun {MyFoldL L F ?R}
    case L
    of nil then R
    [] H|T then {MyFoldL T F {F H R}}
    end
end

fun {Pipeline N}
    P1 P2 P3 in
    P1 = {GenerateList N}
    P2 = {MyFilter P1 fun {$ X} X mod 2 \= 0 end}
    P3 = {MyMap P2 fun {$ X} X*X end}

    {MyFoldL P3 fun {$ X Acc} Acc + X end 0}
end

fun {PipelineThreaded N}
    P1 P2 P3 in
    P1 = thread {GenerateList N} end
    P2 = thread {MyFilter P1 fun {$ X} X mod 2 \= 0 end} end
    P3 = thread {MyMap P2 fun {$ X} X*X end} end

    {MyFoldL P3 fun {$ X Acc} Acc + X end 0}
end

{Browse {SumSquare 10000000}}
{Browse {Pipeline 10000000}}
{Browse {PipelineThreaded 10000000}}