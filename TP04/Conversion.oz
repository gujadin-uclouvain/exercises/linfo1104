declare
fun{Convertir T V}
    fun{F T V}
        if {IsFloat T} andthen {IsInt V} then T*{IntToFloat V}
        elseif {IsInt T} andthen {IsFloat V} then {IntToFloat T}*V
        else T*V
        end
    end
in
    case V
    of state(X Y) then {F T X} + Y
    else {F T V}
    end
end

fun{MetreToFeet T}
    {Convertir T 3.2808}
end

fun{FahrenheitToCelsius T}
    {Convertir T state(0.56 ~17.78)}
end

{Browse {FahrenheitToCelsius 50} }