declare
proc{MakeMulFilter N ?R}
    R = fun{$ I} (I mod N) == 0 end
end
%OU
%fun{MakeMulFilter N}
%    fun{$ I} (I mod N) == 0 end
%end

%{Browse { {MakeMulFilter 5} 6 } }

fun{Filter L F}
    case L
    of nil then nil
    [] H|T then 
        if {F H} then H|{Filter T F}
        else {Filter T F}
        end
    end
end

{Browse {Filter [1 2 3 4 5 6 7 8 9 10] {MakeMulFilter 2} }}